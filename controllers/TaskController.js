//PascalCase
//camelCase
//snake_case
//kebab-case

//Import the Task model from the models folder by using the 'require' directive
const Task = require('../models/Task.js')

// Retrieves all the tasks inside the MongoDB collection
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}

// Creates a new task inside the MongoDB collection
module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then((savedTask, error) => {
		if(error){
			return error
		} 
		return 'Task created successfully!'
	})
}

//Updates an existing task inside the MongoDB collection
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			return error
		}
		result.name = newContent.name

		return result.save().then((updatedTask, error) => {
			if(error){
				return error
			}
			return updatedTask
		})
	})

}

// Deletes a task from the MongoDB Collection 
module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((deletedTask, error) => {
		if(error){
			return error
		}
		return deletedTask
	})
}

// Retrieves a single task from the MongoDB collection
module.exports.findTask = (taskId) =>{
	return Task.findById(taskId).then((foundTask, error) => {
		if(error){
			return error
		}
		return foundTask
	})
}

// Import express and any controllers using the 'require' directive
const express = require('express')
const TaskController = require('../controllers/TaskController.js')
const router = express.Router()

// Route for /api/tasks/ - Runs the getAllTasks function from the controller
router.get('/', (req, res) =>{
	//business logic here
	TaskController.getAllTasks().then(tasks => res.send(tasks))

})

// Route for /api/tasks/create - Runs the createTask function from the controller
router.post('/create', (req, res) => {
	TaskController.createTask(req.body).then((task) => res.send(task)) 
})

// Route for /api/tasks/:id/update - Runs the updateTask function from the controller
// You can use a parameter variable within the endpoint by using a colon ':' and the name of the parameter. Example: ':id'
router.put('/:id/update', (req, res) => {
	// updateTask requires 2 arguments:
	/*
		1. ID of the task to be updated
		2. New content of the task
	*/
	TaskController.updateTask(req.params.id, req.body).then((updatedTask) => res.send(updatedTask))
})

// Route for /api/tasks/:id/delete - Runs the deleteTask function from the controller
router.delete('/:id/delete', (req, res) => {
	TaskController.deleteTask(req.params.id).then((deletedTask) => res.send(deletedTask))
})

// Route for /api/tasks/:id/find - Runs the findTask function from the controller
router.get('/:id/find', (req,res) =>{
	TaskController.findTask(req.params.id).then((foundTask) => res.send(foundTask))
})


module.exports = router




